import { Component, OnInit } from '@angular/core';
import {ReactiveFormsModule, Form, FormGroup, FormsModule, Validators , FormControl, Validator } from '@angular/forms';
import { AbstractControl} from '@angular/forms';
import { FirebaseService } from './../services/firebase.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  form: FormGroup;
  public isSubmit = false;
  public isCreatUser = false;
  
  constructor(private fire: FirebaseService) { }

  ngOnInit() {
    this.form = new FormGroup({
        email: new FormControl('', [ Validators.required, Validators.email]),
        username: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
        password_confirm: new FormControl('', Validators.required)
    }, {
      validators: this.MatchPassword
   });
  }
  register(form) {
    this.isSubmit = true;
    if (this.form.valid) {
    const body = {
        username: form.value.username,
        password: form.value.password,
        email: form.value.email
      };
      console.log(body);
       this.fire.createUser(body).then(
          res => {
            //this.form.reset();
            console.log(res);
            this.isSubmit = false;
            this.isCreatUser = true;
            setTimeout(function() {
               this.isCreatUser = false;
            }, 3000);
            
          }
    )
    } else {
      console.log('invalid form');
    }
  }

  MatchPassword(AC: AbstractControl) {
    const pass = AC.get('password').value;
    const cPassword = AC.get('password_confirm').value;
     if (pass !== cPassword) {
         AC.get('password_confirm').setErrors( {MatchPassword: true});
     } else {
         return null;
   }
 }

}