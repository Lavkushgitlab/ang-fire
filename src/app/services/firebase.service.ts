import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  public userProfileRef:firebase.database.Reference;
  constructor(public fireDB: AngularFirestore) {
     this.userProfileRef = firebase.database().ref('/users');
   }
  createUser(modal) {
    return this.fireDB.collection('users').add({
      username: modal.username,
      email: modal.email,
      password: modal.password
    });

    //  return firebase.auth().createUserWithEmailAndPassword(modal.email,modal.password)
    //                        .then(user => {
    //                         console.log(user);
    //                           var ref = firebase.database().ref().child("accounts");
    //                           var data = {
    //                               username: modal.username,
    //                               email: modal.email
    //                              }
    //                           ref.child(user.user.uid).set(data).then(function(ref) {//use 'child' and 'set' combination to save data in your own generated key
    //                               console.log("Saved");
    //                              }, function(error) {
    //                               console.log(error); 
    //                           });
                              
    //                         })
    //                         .catch(function(error) {
    //                           var errorCode = error.code;
    //                           var errorMessage = error.message;
    //                           if (errorCode == 'auth/weak-password') {
    //                               alert('The password is too weak.');
    //                           } else if (errorCode == 'auth/email-already-in-use') {
    //                               alert('The email is already taken.');
    //                           } else if (errorCode == 'auth/weak-password') {
    //                               alert('Password is weak');
    //                           } else {
    //                               alert(errorMessage);
    //                           }
    //                           console.log(error);
    //                                 // ...
    //                         }); 
  }

 

  userLogin(modal){
    // return this.fireDB.collection('users').add({
    //   username: modal.username,
    //   email: modal.email,
    //   password: modal.password
    // });
  }
  
userSignIn(modal) {
  let pass: string = modal.password ;
  let email: string = modal.email ;
  //   code: "auth/argument-error"
  // message: "signInWithEmailAndPassword failed: Second argument "password" must be a valid string."
  //service:::: code: "auth/wrong-password"
  //message: "The password is invalid or the user does not have a password."
  return firebase.auth().signInWithEmailAndPassword( email,pass).then((authData) => {
        console.log(authData);
  }).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // ...
    console.log('service::::',error);
  });
}
  

  getUsers(){
    return this.fireDB.collection('users').snapshotChanges();
  }

  deleteUser(userKey){
    return this.fireDB.collection('users').doc(userKey).delete();
  }

  getUser(userKey){
    return this.fireDB.collection('users').doc(userKey).snapshotChanges();
  }

}




// signup(email: string, password: string, firstname: string, lastname, 
//     nickname: string, address: string) {
//  this.firebaseAuth
//   .auth
//   .createUserWithEmailAndPassword(email, password)
//   .then(value => {
//     this.u = value.user;
//     this.u.updateProfile({ displayName: nickname, photoURL: null });
//     this.firebaseAuth.auth.updateCurrentUser(this.u);
//     var actionCodeSettings = {
//       url: 'https://www.example.com/?email=' + this.u.email,
//       handleCodeInApp: true,
//     };
//     this.u.sendEmailVerification(actionCodeSettings);

//     console.log('Success!', value);
//   })
//   .catch(err => {
//     console.log('Something went wrong:', err.message);
//   });
//  }
