import { Component, OnInit } from '@angular/core';
import { FirebaseService } from './../services/firebase.service';
import {ReactiveFormsModule, Form, FormGroup, FormsModule, Validators , FormControl, Validator } from '@angular/forms';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  form: FormGroup;
  allUsers: any;
  email:any = '';
  username:any = '';
  public isEdit: boolean = false;
  constructor(private fire: FirebaseService) { }
  ngOnInit() {
     this.form = new FormGroup({
        email: new FormControl('', [ Validators.required, Validators.email]),
        username: new FormControl('', Validators.required),
    });
      this.getAllUsers();
  }
  getAllUsers(){
    this.fire.getUsers().subscribe( res => { 
        this.allUsers = res;
        console.log(res);
    });            
  }
deleteUser(id){
    this.fire.deleteUser(id)
    .then(
      res => { console.log('res',res);
      },
      err => { console.log(err); }
    )
}

editUserData(userId,index){
    this.isEdit = true;
    this.fire.getUser(userId)
      .subscribe(
        (data: any) => {
          if(data){
              this.username = data.payload.data().username;
              this.email = data.payload.data().email;   
          }
            
        }
      );
 }
}
