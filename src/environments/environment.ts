// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyDI-f3NsSl3mfArCgbAVk_RbcQcuva4Msg",
    authDomain: "angular-blast.firebaseapp.com",
    databaseURL: "https://angular-blast.firebaseio.com",
    projectId: "angular-blast",
    storageBucket: "angular-blast.appspot.com",
    messagingSenderId: "574214626589",
    appId: "1:574214626589:web:0e441dfca945fca0fd95f4",
    measurementId: "G-0R3E1N1WR4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
